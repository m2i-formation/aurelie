package tp_aurelie;

import java.util.concurrent.TimeUnit;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class tp {
	public static ChromeDriver driver;
	public static String nom;

	@BeforeClass
	public static void openDriver() {
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

	}

	@Test
	public void a_connexion() {
		driver.get("https://demo.glpi-project.org");
		driver.findElement(By.id("login_name")).sendKeys("admin");
		driver.findElement(By.id("login_password")).sendKeys("admin");

		new Select (driver.findElement(By.name("language"))).selectByVisibleText("Fran�ais");
		
		driver.findElement(By.name("submit")).click();

		driver.findElement(By.linkText("Parc")).click();
		driver.findElement(By.linkText("Ajouter")).click();
		driver.findElement(By.linkText("Gabarit vide")).click();
		driver.findElement(By.name("name")).sendKeys("ordi-aurelie");
		nom = driver.findElement(By.name("name")).getText();
		System.out.println(nom);
		String numSerie = this.getRandomReference();
		driver.findElement(By.name("serial")).sendKeys(numSerie);
		// Ajout Domaine
		driver.findElement(
				By.xpath("//span[@class='select2-selection__rendered']")).click();
		// Ajout commentaires
		driver.findElement(By.id("comment")).sendKeys("Voici un nouvel ordi");
		// Ajout domains
		driver.findElement(By.name("domains_id")).click();
		driver.findElement(By.xpath("//span[@role='presentation']")).click();
	

		driver.findElement(By.name("globalsearch")).sendKeys(nom);
		driver.findElement(By.xpath("//i[@class='fa fa-search']")).click();

	}

	@Test
	public void b_ajouterOrdi() {

	}

	@Test
	public void c_deconnexion() {

	}

	public String getRandomReference() {
		String ref = "";
		String allowedChars = "ABCDEFGHIJKLMNOPQRSTUVWYZ";
		int stringLength = 4;
		for (int i = 0; i < stringLength; i++) {
			int rnum = (int) Math.floor(Math.random() * allowedChars.length());
			ref += allowedChars.substring(rnum, rnum + 1);
		}
		ref += "-";
		for (int i = 0; i < stringLength; i++) {
			int rnum = (int) Math.floor(Math.random() * 9);
			ref += rnum;
		}
		return ref;
	}

	@AfterClass
	public static void tearDown() {
		// driver.quit();

	}

}
